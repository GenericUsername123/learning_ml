# Intro to Machine Learning

## /tools/startup.py (Enron data)

Om alle projecten te kunnen draaien is de Enron email dataset nodig.
Om deze data te downloaden en te unzippen moet je de script /tools/startup.py draaien.
Dit komt neer op 400MB+ en zal een geruime tijd duren.
**De eerste miniprojecten maken geen gebruik van deze data.**