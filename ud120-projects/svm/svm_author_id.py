#!/usr/bin/python

""" 
    This is the code to accompany the Lesson 2 (SVM) mini-project.

    Use a SVM to identify emails from the Enron corpus by their authors:    
    Sara has label 0
    Chris has label 1
"""
    
import sys
from time import time
sys.path.append("../tools/")
from email_preprocess import preprocess


### features_train and features_test are the features for the training
### and testing datasets, respectively
### labels_train and labels_test are the corresponding item labels
features_train, features_test, labels_train, labels_test = preprocess()




#########################################################
### your code goes here ###

from sklearn import svm
from time import time

def classify(features_train, labels_train, kernel = "rbf", C = 1.0):
    clf = svm.SVC(kernel=kernel, C=C)
    t0 = time()
    clf.fit(features_train, labels_train)
    print "training time:", round(time() - t0, 3), "s"
    t0 = time()
    pred = clf.predict(features_test)
    print "prediction time:", round(time() - t0, 3), "s"
    score = clf.score(features_test, labels_test)
    return pred, score

def resize_set(set, divider = 1):
    return set[:len(set)/divider]

# Q1
# pred, score = classify(features_train, labels_train, "linear", 1.0)
# print "Score:", score

# Q2
# pred, score = classify(
#     features_train=resize_set(features_train, 100),
#     labels_train=resize_set(labels_train, 100),
#     kernel="linear"
# )
# print "Score:", score

# Q3
# pred, score = classify(
#     features_train=resize_set(features_train, 100),
#     labels_train=resize_set(labels_train, 100),
#     kernel="rbf"
# )
# print "Score:", score

# Q4 & Q5
# pred, score = classify(
#     features_train=resize_set(features_train, 100),
#     labels_train=resize_set(labels_train, 100),
#     kernel="rbf",
#     C=10
# )
# print "Score C=10:", score
# pred, score = classify(
#     features_train=resize_set(features_train, 100),
#     labels_train=resize_set(labels_train, 100),
#     kernel="rbf",
#     C=100
# )
# print "Score: C=100", score
# pred, score = classify(
#     features_train=resize_set(features_train, 100),
#     labels_train=resize_set(labels_train, 100),
#     kernel="rbf",
#     C=1000
# )
# print "Score: C=1000", score
# pred, score = classify(
#     features_train=resize_set(features_train, 100),
#     labels_train=resize_set(labels_train, 100),
#     kernel="rbf",
#     C=10000
# )
# print "Score: C=10000", score

# Q6
# pred, score = classify(
#     features_train=resize_set(features_train),
#     labels_train=resize_set(labels_train),
#     kernel="rbf",
#     C=10000
# )
# print "Score: C=10000", score

# Q7
# pred, score = classify(
#     features_train=resize_set(features_train, 100),
#     labels_train=resize_set(labels_train, 100),
#     kernel="rbf",
#     C=10000
# )
# for item in [10, 26, 50]:
#     print item, "-", pred[item]

# Q8
pred, score = classify(
    features_train=resize_set(features_train),
    labels_train=resize_set(labels_train),
    kernel="rbf",
    C=10000
)
print "Chris count:", sum(1 for i in pred if i == 1)

#########################################################


