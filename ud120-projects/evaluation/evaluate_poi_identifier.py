#!/usr/bin/python


"""
    Starter code for the evaluation mini-project.
    Start by copying your trained/tested POI identifier from
    that which you built in the validation mini-project.

    This is the second step toward building your POI identifier!

    Start by loading/formatting the data...
"""

import pickle
import sys
sys.path.append("../tools/")
from feature_format import featureFormat, targetFeatureSplit

data_dict = pickle.load(
    open("../final_project/final_project_dataset.pkl", "r"))

# add more features to features_list!
features_list = ["poi", "salary"]

data = featureFormat(data_dict, features_list)
labels, features = targetFeatureSplit(data)


# your code goes here

from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import recall_score, precision_score
import numpy as np

x_train, x_test, y_train, y_test = [
    np.array(i) for i in train_test_split(features, labels, test_size=.3, random_state=42)
]
clf = DecisionTreeClassifier().fit(x_train, y_train)
pred = np.array(clf.predict(x_test))
print "Classified POIs:", (pred == 1.).sum()
print "People in test set:", y_test.size
print "True positives:", (y_test[np.where(pred == 1)[0]] == 1).sum()
print "Precision:", precision_score(y_test, pred)
print "Recall:", recall_score(y_test, pred)
predictions = np.array([0, 1, 1, 0, 0, 0, 1, 0, 1, 0,
                        0, 1, 0, 0, 1, 1, 0, 1, 0, 1])
true_labels = np.array([0, 0, 0, 0, 0, 0, 1, 0, 1, 1,
                        0, 1, 0, 1, 1, 1, 0, 1, 0, 0])
tp, tn, fp, fn = [
    (true_labels[np.where(predictions == 1)[0]] == 1).sum(),
    (true_labels[np.where(predictions == 0)[0]] == 0).sum(),
    (true_labels[np.where(predictions == 1)[0]] == 0).sum(),
    (true_labels[np.where(predictions == 0)[0]] == 1).sum()
]
print "True positives (practice):", tp
print "True negatives (practice):", tn
print "False positives (practice):", fp
print "False negatives (practice):", fn
print "Precision score (practice):", tp / float(tp + fp)
print "Recall score (practice):", tp / float(tp + fn)
