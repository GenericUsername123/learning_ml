=================================================================================

Persoonlijk ontwikkelingsplan (POP) - opvolgingsdocument Academiejaar 2017-2018

=================================================================================


Student:	Mathias Grosse

Mentor:		Joris Maervoet


=================================================================================
Beschrijving - datum 17/02/2018
=================================================================================

Onderwerp:

De fundamenten van Machine Learning leren kennen en toepassen a.d.h.v. Udacity


Einddoelen (met evaluatiecriteria):

1. Alle projecten van de cursus "Intro to Machine Learning" (~60h) en "Deep Learning" (~80h) afwerken en deze beschikbaar maken op GIT.
2. Alle antwoorden van beide cursussen noteren.
3. Alle demonstreerbare projecten documenteren (meerdere projecten in cursus "Deep Learning" kunnen gedemonstreerd worden).


Motivatie - betrokkenheid:

Dit is een technologie dat vooral de laatste 2 jaar snel evolueerde met nog veel ongekende toepassingen. Dit is iets dat me mogelijks niet veel zal interesseren, maar de nieuwsgierigheid achter dit onderwerp is zeker genoeg om meer dan 100 uur te vullen.

Motivatie - grenzen verleggen:

Dit is een compleet onbekend terrein voor mij al heb ik eerder met Weka gewerkt. Hieruit had ik wel wat kunnen leren over ML, maar dit project was meer over leren onderzoeken (voor mij).
Pathfinding (zoals A*, DFS, BFS) is een onderwerp dat ik al kan toepassen, maar dit zal vermoedelijk niet  overlappen.

Motivatie - professioneel profiel:

Dit is een technologie dat je op bijna alles in het dagelijks leven kan toepassen, vooral in de velden waarin ik me specialiseer.

1.c. De student realiseert onderstaande kerndoelen binnen het deelgebied van de netwerkinfrastructuur en datacentertechnologie (afstudeerrichting ICT).
2.a. De student kan zelfstandig een complex technologisch probleem opsplitsen in een aantal concrete en hanteerbare deelproblemen.
2.b. De student kan voor een concreet probleem een oplossing ontwikkelen die voldoet aan de vooropgestelde eisen.
3.a. De student kan de goede werking van een systeem beoordelen en de juiste instellingen of configuratie aanbevelen.
3.b. De student kan de goede werking van een complex systeem evalueren.
4.a. De student kan geschikte testen en metingen selecteren en uitvoeren om een vastgesteld probleem te onderzoeken.
4.b. De student kan op basis van een beoordeling van test-en meetresultaten de nodige acties en aanbevelingen voorstellen om fouten op te lossen of de werking van systemen (inclusief de beveiliging ervan) te optimaliseren.
5.c. De student kan op basis van eigen onderzoek en metingen geschikte parameters en componenten selecteren om een werkbare oplossing te construeren voor een niet vertrouwd complex probleem
6.e. De student kan een voorgestelde aanpak, oplossing of aanbeveling op een gestructureerde manier verantwoorden op basis van eigen onderzoek, kennis en inzicht en beoordeling van voor-en nadelen.
8.i. De student maakt een duidelijk verslag van uitgevoerde taken en opgestelde conclusies.
8.j. De student kan een heldere, gestructureerde, volledige en bruikbare documentatie opstellen van ontwikkelde systemen en oplossingen.
8.k. De student kan zowel mondeling als schriftelijk een heldere presentatie geven van een onderzoek of ontwerp, aangepast aan het doelpubliek.
9.l. De student kan de gevolgen van zijn eigen handelen voorspellen en beoordelen in de maatschappelijke en juridische context van de eigen werkomgeving.
10.m. De student kan tekorten in de eigen kennis of competentie identificeren en neemt initiatief om ze te remediëren.

Werkvorm:
De student leert op basis van MOOCs hoe Machine Learning waaronder Deep Learning of Reinforcement Learning toe te passen in meerdere probleemstellingen. Het leerproces wordt gedocumenteerd en meerdere PoCs worden gedemonstreerd om verschillende probleemstellingen en technieken aan te tonen.


Goedkeuring mentor 20/02/2018
-----------------------------
Goedgekeurd op voorwaarde dat je voldoende bewijs bijhoudt van je vorderingen, dat je de projecten 
regelmatig synct op GIT en dat het eigen werk is.

ps: je zal al een vette kluif hebben aan "Intro to Machine Learning" (je moet ook wat Python en 
statistiek kunnen). Je mag bij de cursus "Deep learning" gerust vermelden dat het een mogelijke 
uitbreiding is.

Veel succes!!

=================================================================================
Planning - 25/02
=================================================================================

De planning is opgedeeld in de twee cursussen vermeld in het voorstel.
Eerst een indeling van de cursus, daarna pas de tijdlijn.

Een onderdeel is pas afgewerkt als dit gedocumenteerd is in de GIT-repository.
D.w.z. dat alle scripts en resources dat nodig zijn om dit deel te begrijpen zich in deze repository bevinden.

== Cursus "Intro to Machine Learning" (eerste 7 weken) ==

-- Nodige kennis --
* Python (basis)
* Statistiek (basis)

-- Delen (17) --
(basis)
* Intro
* Naive Bayes
* SVM
* Decision Trees

(research & problemsolving)
* Choose Your Own
* Datasets and Questions

(new concepts)
* Regressions
* Outliers
* Clustering
* Feature Scaling

(optimization)
* Text Learning
* Feature Selection
* PCA
* Validation
* Evaluation Metrics

(PoC)
* Trying It All Together
* Final Project

== Planning deel 1 ==
Week 3:
* De GIT-repository van "Intro to Machine Learning" toevoegen aan die van POP.
* De eerste vier delen afwerken (basis) van de eerste cursus.
* Al een eerste deel afwerken van het stuk research & problemsolving.
Week 4:
* research & problemsolving afwerken.
* new concepts afwerken
Week 5:
* de eerste 2 delen van optimization afwerken
Week 6:
* het deel PCA & Validation afwerken
Week 7:
* optimization afronden
* PoC afwerken en documenteren

== Cursus "Deep Learning" (laatste 5 weken) ==

-- Nodige kennis --
* Python (kunnen werken met Pandas & Numpy)
* Machine Learning (Intro to Machine Learning word vermeld in prerequisites)
* Multivariabele vergelijkingen
* Product van een matrix

-- Delen (7+outro) --
(basis)
* From Machine Learning to Deep Learning
* Assignment: notMNIST

(dl concepts)
* Deep Neural Networks
* Convolutional Neural Networks
* Deep Models for Text and Sequences

(PoC)
* Software and Tools
* Build a live Camera App

== Planning deel 2 ==
Week 8:
* GIT-repository: Deep Learning toevoegen
* basis afwerken
Week 9:
* dl concepts: neuraal netwerk afwerken
Week 10:
* dl concepts: Convolutional Neural Networks afwerken
Week 11:
* dl concepts: Deep Models for Text and Sequences
* begin aan PoC deel 1
Week 12:
* PoC afwerken

=================================================================================
Logboek
=================================================================================

25/03 - ud170-projects toegevoegd en aangevult tot en met L1:19 (Lesson 1, Item 19)
27/03-28/03 - ud170-projects: L1&2 afgewerkt
31/03 - ud170-projects: aangevult tot en met L3:6
01/04 - ud170-projects: aangevult tot en met L3:13
02/04 - ud170-projects: aangevult tot en met L3:16
04/04 - ud170-projects: aangevult tot en met L3:17
05/04 - ud170-projects: L3 afgewerkt
06/04 - ud120-projects: aangevult tot en met PCA (+ theorie Validation)
07/04 - ud120-projects: optimization afgewerkt & start PoC
08/04 - ud120-projects: PoC: uitgepland & bezig met analyseren van de dataset
09/04 - ud120-projects: PoC: dataset verwerken
10/04 - ud120-projects: PoC: preprocessing in orde
12/04 - ud120-projects: PoC: uitbreiding van preprocessing en het
implementeren van een feedbackloop tussen classificatie en unsupervised preprocessing
(TL;DR supervised classification proberen omzetten naar unsupervised classification)
13/04 - ud120-projects: PoC: complete rewrite met sklearn's preprocessing
14/04 - ud120-projects: feature- en model-selection upgrade & classification tuning
15/04 - ud120-projects: PoC: feature- en model-selection upgrade
16/04 - ud120-projects: PoC: tweaked scorer & meer classifiers (model-selection upgrade)
17/04 - ud120-projects: PoC: GridSearchSV scorer fix (model-selection upgrade)
18/04 - ud120-projects: PoC: PCA & scaler order fix & classification tuning upgrade
19/04 - ud120-projects: PoC: ml_logger module geïmplementeerd (classificatie upgrade)
20/04 - ud120-projects: PoC: ml_logger update & switches toegevoegd aan poi_id
21/04 - ud120-projects: PoC: verschillende evaluation metrics vergeleken (eerste classifier geöptimaliseerd)
30/04 - ud120-projects: PoC: alle classifiers getest (uitzondering: rfc) & ud170-projects: PoC gestart
01/05 - ud170/120-projects: PoC: afgewerkt & ud730-projects start
26/05 - ud730-projects: L1: downloader van notMNIST bijna afgewerkt
27/05 - ud730-projects: L1: notMNIST afgewerkt

=================================================================================
Eerste tussenrapportering - 23/03
=================================================================================

Stand van zaken:

Op dit moment is alles nog vlot verlopen al ben ik niet op schema (voor maandag plan ik ~8h werk, ~4h te kort om op schema te blijven).
Ik heb mijn planning moeten wijzigen door statistieken bij te nemen voor zowel DL als intro in ML. (Dit neemt ongeveer een anderhalve week uit de planning.)
Als ik op dezelfde tempo werk als voorheen zou ik een week of een anderhalve week missen, maar ik plan weer op schema te komen, ten laatste, tegen week 10. (Dit is haalbaar.)

De scripts nodig voor statistiek (het essentiële) horen geüpload te zijn voor maandag.
(Ik plan enkel uit deze cursus te halen wat ik nodig heb om mij weer op ML te kunnen focussen. Mogelijks is dit niet de volledige cursus.)

Cursus: Intro to data analysis (https://eu.udacity.com/course/intro-to-data-analysis--ud170)

Reflectie:

Ik denk dat ik bij mijn oude planning zou kunnen blijven, maar ik spaar mogelijks meer tijd door toch een cursus statistieken bij te nemen.
Tot nu toe heb ik alles zeer positief ervaren en ik heb mijn werk altijd in blokken van 5-8h per dag gedaan tijdens het week-end, maar ik kan gerust minstens een volle werkdag bijnemen.
Ik heb er geen spijt van en ik denk dat ik erin zal slagen op zijn minst een poging te doen aan mijn PoC voor DL.

Zelfevaluatie:

Scores volgens het evaluatiedocument:
Nieuw verworven kennis: A
Toepassing aangetoond: C (motivatie: nog geen PoCs)
Planning: B
Initiatief - inzet: C
Rapportering: C


Feedback vanwege mentor - 30/03/2018:

Dag Mathias,
je manier van werken (gestructureerde aanpak), je planning en de stijl van je tussenrapportering zijn uitstekend !!!
Het is inderdaad de bedoeling dat je je planning aan je noden aanpast - dus doe gerust en zie wat er mogelijk is.

Steek een tandje bij, denk aan de PoCs en ik denk wel dat je dan een goed POP-project oplevert.

Wat beter kan is je logboek: het is de bedoeling dat je daar per week aangeeft wat je van je planning realiseert. 

Akkoord met je zelfevaluatie. Rapportering lijkt me voorlopig beter dan een C.

veel succes met het verdere verloop !!



=================================================================================
Eindrapportering - 27/05
=================================================================================


Eindrapportering:

* De cursus "Intro to Machine Learning" is afgewerkt inclusief de PoC.
* De cursus "Intro to Data Analysis" (een toegevoegde cursus) is afgewerkt inclusief de PoC.
* De cursus "Deep Learning" is niet volledig. Enkel les 1 is afgewerkt.

Een lichte documentatie over de belangrijkste projecten kan gevonden worden in de README.md files.

Reflectie:

Ik had heel wat in mijn originele einddoelen en was ervan overtuigd dat het me enkel zou lukken als ik er druk op houdt.
Dit was geen probleem, maar ik had minder tijd dan verwacht en het leek mij beter een extra cursus te volgen, wat nog meer in gedrang kwam.

Enkele punten waaraan ik wil werken:
* Een meer voorzichtige planning opstellen.
* Meer research doen voor ik aan een eindproject begin. (Zie ud120*/final_project/README.md)
* Ik had verwacht meer af te kunnen werken van Deep Learning zelfs na het toevoegen van Data Analysis.

Enkele positieve punten:
* Ik was consistent met als uitzondering in mei (dit door andere deadlines waarmee geen rekening werd gehouden).
* Ik heb énorm veel bijgeleerd en mijn tijd is zeker wel besteed.
* Dit was een positieve ervaring van het grootste project dat ik ooit begonnen ben.
Nog nooit heb ik zoveel uren aan één project besteed en ik ben van plan hier ook aan verder te werken.

Al bij al denk ik dat dit een geslaagd project is al is het niet volledig volgens planning gegaan en heb ik minder van Deep Learning kunnen leren dan ik wou.

Inhoud Portfolio:

* ud120-projects (Intro to Machine Learning)
  * folders per onderwerp (instructies bevinden zich in de README.md files)
* ud170-projects (Intro to Data Analysis)
  * folders per les
* ud730-projects (Deep Learning)
  * L1 (les 1)
    * scripts die tijdens de les geschreven zijn
    * folder: notMNIST, een mini-project (zie README.md)

De PoC's bevinden zich in final_project onder de project-folder van de cursus (vb. ud120-projectes/final_project)
Alle scripts van elke les zijn gedocumenteerd (folders zijn gestructureerd per onderwerp of per les).
Voor meer context rond deze scripts zal het nodig zijn een account aan te maken op Udacity.com (dit is gratis).

De links van elke cursus (inschrijven is gratis):
* Machine Learning: (https://eu.udacity.com/course/intro-to-data-analysis--ud120)
* Data Analysis: (https://eu.udacity.com/course/intro-to-data-analysis--ud170)
* Deep Learning: (https://eu.udacity.com/course/intro-to-data-analysis--ud730)

Bronnenlijst:

[1] Udacity, "Intro to Machine Learning", 2017. [Online]. Available: https://classroom.udacity.com/courses/ud120. [Geopend 27 Mei 2018]
[2] Udacity, "Intro to Data Analysis", [Online]. Available: https://classroom.udacity.com/courses/ud170. [Geopend 27 Mei 2018]
[3] Udacity, "Intro to Deep Learning", [Online]. Available: https://classroom.udacity.com/courses/ud730. [Geopend 27 Mei 2018]
[4] Udacity, "Discussion Forum", [Online]. Available: https://discussions.udacity.com. [Geopend 27 Mei 2018]
[5] scikit-learn, "Documentation of scikit-learn 0.19.1", [Online]. Available: http://scikit-learn.org/stable/documentation.html. [Geopend 27 Mei 2018]
[6] michealreinhard, "Final project restart: Vivek", 2 Mei 2016. [Online]. Available: https://github.com/michaelreinhard/ud120-projects/blob/master/final_project/final_project_restart_vivek.ipynb. [Geopend 27 Mei 2018]
[7] Data Talks, "Scikit Learn Intro", 29 Mei 2017. [Online]. Available: https://www.youtube.com/watch?v=52zXNBaUKMM&list=PLgJhDSE2ZLxb33q-x5592LCiVRsHDxVf3. [Geopend 27 Mei 2018]
[8] mcchangun, "sklearn GridSearchCV", 10 Januari 2014. [Online]. Available: https://stackoverflow.com/questions/21050110/sklearn-gridsearchcv-with-pipeline. [Geopend 27 Mei 2018]
[9] Dan Carter, "How to use the output of GridSearch?", 1 Augustus 2017. [Online]. Available: https://datascience.stackexchange.com/questions/21877/how-to-use-the-output-of-gridsearch. [Geopend 27 Mei 2018]
[10] Katie M., "Workflows in Python: Using Pipeline and GridSearchCV for More Compact and Comprehensive Code", 6 Januari 2016. [Online]. Available: https://www.civisanalytics.com/blog/workflows-in-python-using-pipeline-and-gridsearchcv-for-more-compact-and-comprehensive-code/. [Geopend 27 Mei 2018]

Zelfevaluatie:

Scores volgens het evaluatiedocument:
Nieuw verworven kennis: A
Toepassing aangetoond: C (motivatie: Deep Learning is niet afgewerkt, maar het idee is verwezenlijkt)
Planning: C (motivatie: andere deadlines en onverwachte omstandigheden waren niet ingecalculeerd)
Initiatief - inzet: A
Rapportering: A