# Report

## Hypothesis

In the Titanic help was too late for many so it all came down to who was prioritized more by the rescuers aboard.
This means that your only chance of survival is by being someone important to them.
There are two main reasons why they would prioritize you.
One is when you have an importance in society (e.g. mother) and one is when you have a fuller live ahead of you (i.e. child).
I would expect there to be more than one exception to this. A couple of these exceptions would be more vulnerable people of any age and wealthy people at a less desperate moment.

Something that would negatively impact your chances would be being a third class passenger. I expect their sections in the ship to be more crowded and possibly even more unsafe.

What I would expect is to see a better chance of survival with children, their mothers and wealthy people.

## Findings

It seems like the data somewhat meets expectations. An exception to this is that women have a very high chance of survival, more so than children and more than what I expected. I expected to see a higher number of men as it is hard to leave someone behind. As for why chances for women are higher than for children: women are more mobile than children, can't get lost and are tougher than children resulting in a higher chance.

An other unexpected statistic is that people who have a family of four are very likely to be saved. Two of the four family members probably where children most of the time and a mother would than be the third. This would mean that if all families are saved except for the father: it would result in a ~75% chance. The actual number is ~72%. The survival chances for women between 25 years old and 40 years old was ~80%. 3/4 of 80% is 60%, so it is likely that often the father was saved too despite the prioritization for women and children. This could be because of two reasons. One would be that complete families would be a priority also (but chances of smaller & bigger families are lower) and an other more likely explanation would be that it often was impossible to leave the father on the Titanic.

### When looking at age groups

*At first I thought this could be because young adults where a majority, but this was not the case. Older adults, the ones who have a higher chance of survival, are the majority. I realized however that numbers would probably not affect chance of survival more than with others. It would minimize chances for everyone.*

* Children had the highest chance of survival.
* Young adults (age 14-25) had a relatively low chance of survival.
* Older adults (age 25-40) had a relatively high chance of survival.
* Older people (age 40-100) had a small chance of survival.

The most likely explanation for this would be that young adults are negatively affected because of the prioritization for children and women (preferably their mothers). The statistics show an increase of chance of survival for women among the age group for older adults (age 25-40). This could be because those women are for a large part mothers. Women of ages 14 to 25 probably didn't have children that often.

## Conclusion

The most noticeable thing was that being wealthy boosted your chances. Just by looking at correlations to survival two thing popped out. Survival was correlated to the passenger class and a little less to general wealth. To really speculate on this it would be important to know the layout as it is possible that first class passengers where located better for survival. However, other factors can affect this too.

It really shows that women and children where prioritized more. This reflects more in the survival chances for women however, as survival chances for children are smaller probably due to the conditions they where exposed to.
Chances of survival where low for men, but high for boys as one would expect to see due to the prioritization.