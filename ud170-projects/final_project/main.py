import sys
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

rows, columns = os.popen("stty size", "r").read().split()
pd.set_option("display.max_rows", int(rows))
pd.set_option("display.width", int(columns))


def print_header(padding, header, length=80):
    pCount = (length - len(header) - 2) / 2
    header = padding * pCount + " " + header + " " + padding * pCount
    print "\n" + padding * len(header)
    print header
    print padding * len(header), "\n"


titanic_ds = pd.read_csv("titanic_data.csv")
titanic_ds.set_index("PassengerId", inplace=True)
titanic_ds.Survived = titanic_ds.Survived.astype(bool)

print_header("=", "Titanic dataset")

# start of making features

# normaly you first identify any outliers

print_header("-", "Making new features")


def min_max_scale(ds):
    return (ds - ds.min()) / (ds.max() - ds.min())


def invert(ds):
    return ds.max() - ds + 1


indicator = min_max_scale(titanic_ds.Fare) * \
    invert(titanic_ds.Pclass) + invert(titanic_ds.Pclass)
titanic_ds["wealthy"] = indicator > indicator.mean()
titanic_ds["family_size"] = titanic_ds.Parch + titanic_ds.SibSp + 1
titanic_ds["alone"] = titanic_ds.family_size == 1

# end of making features

print_header("-", "Column descriptions")
print "Pclass: Ticket class"
print "SibSp: # of siblings/spouses aboard"
print "Parch: # of parents/children aboard"

print_header("-", "Count")
print titanic_ds.count().sort_values()

print_header("-", "Looking for outliers")
print titanic_ds.describe()


def get_likeliness(by):
    df = titanic_ds.set_index(by) if type(by) is str else titanic_ds
    return df.Survived.groupby(by).mean().sort_values(ascending=False)


def get_likeliness_df(by, df):
    return df.Survived.groupby(by).mean().sort_values(ascending=False)


# This contains the most important info

print_header("-", "Numerical correlations")
corr = titanic_ds.corr()
print corr[corr.abs() > .3].replace(1, np.nan)

print_header("-", "Likeliness of survival by feature")
print get_likeliness("Sex")
print get_likeliness("family_size")
print get_likeliness("wealthy")
print get_likeliness("alone")
ages = [0, 14, 25, 40, 60, 100]
print get_likeliness(pd.cut(titanic_ds.Age, ages))
df = titanic_ds[titanic_ds.Sex == "female"]
print get_likeliness_df(pd.cut(df.Age, ages), df)
