# First iteration

qkey = "account_key"

# CSVs in Python
import unicodecsv


def read_csv(filename):
    with open(filename, "rb") as f:
        reader = unicodecsv.DictReader(f)
        return list(reader)


enrollments = read_csv("resources/enrollments.csv")
daily_engagement = read_csv("resources/daily_engagement.csv")
project_submissions = read_csv("resources/project_submissions.csv")

# Fixing Data Types

from datetime import datetime as dt


def parse_date(date):
    if date == '':
        return None
    else:
        return dt.strptime(date, '%Y-%m-%d')


def parse_maybe_int(i):
    if i == '':
        return None
    else:
        return int(i)


for enrollment in enrollments:
    enrollment['cancel_date'] = parse_date(enrollment['cancel_date'])
    enrollment['days_to_cancel'] = parse_maybe_int(
        enrollment['days_to_cancel'])
    enrollment['is_canceled'] = enrollment['is_canceled'] == 'True'
    enrollment['is_udacity'] = enrollment['is_udacity'] == 'True'
    enrollment['join_date'] = parse_date(enrollment['join_date'])

enrollments[0]
for engagement_record in daily_engagement:
    engagement_record['lessons_completed'] = int(
        float(engagement_record['lessons_completed']))
    engagement_record['num_courses_visited'] = int(
        float(engagement_record['num_courses_visited']))
    engagement_record['projects_completed'] = int(
        float(engagement_record['projects_completed']))
    engagement_record['total_minutes_visited'] = float(
        engagement_record['total_minutes_visited'])
    engagement_record['utc_date'] = parse_date(engagement_record['utc_date'])

daily_engagement[0]
for submission in project_submissions:
    submission['completion_date'] = parse_date(submission['completion_date'])
    submission['creation_date'] = parse_date(submission['creation_date'])

project_submissions[0]

# Investigating the Data


def getUnique(list, key="account_key"):
    uniques = set()
    for item in list:
        uniques.add(item[key])
    return uniques


enrollment_num_rows = len(enrollments)
enrollment_num_unique_students = len(getUnique(enrollments))
engagement_num_rows = len(daily_engagement)
engagement_num_unique_students = len(getUnique(daily_engagement, "acct"))
submission_num_rows = len(project_submissions)
submission_num_unique_students = len(getUnique(project_submissions))

# Problems in the Data
for i, engagement in enumerate(daily_engagement):
    daily_engagement[i][qkey] = engagement["acct"]
print "Key of first daily_engagement entry:", daily_engagement[0][qkey]

# Missing Engagement Records
uniq_engagement_students = set()
for item in daily_engagement:
    uniq_engagement_students.add(item[qkey])

exceptions = [
    enrollment for enrollment in enrollments
    if enrollment[qkey] not in uniq_engagement_students
]
print exceptions[0]

# Checking for More Problem Records
for enrollment in exceptions:
    if enrollment["join_date"] != enrollment["cancel_date"]:
        print enrollment

# Tracking Down the Remaining Problems
udacity_test_accounts = set()
for enrollment in enrollments:
    if enrollment['is_udacity']:
        udacity_test_accounts.add(enrollment['account_key'])


def remove_udacity_accounts(data):
    non_udacity_data = []
    for data_point in data:
        if data_point['account_key'] not in udacity_test_accounts:
            non_udacity_data.append(data_point)
    return non_udacity_data


non_udacity_enrollments = remove_udacity_accounts(enrollments)
non_udacity_engagement = remove_udacity_accounts(daily_engagement)
non_udacity_submissions = remove_udacity_accounts(project_submissions)

# Refining the Question

paid_students = {}
for enrollment in non_udacity_enrollments:
    if not enrollment["is_canceled"] or int(enrollment["days_to_cancel"]) > 7:
        key = enrollment[qkey]
        if key not in paid_students or enrollment["join_date"] > paid_students[key]:
            paid_students[key] = enrollment["join_date"]
print "Length paid_students:", len(paid_students)

# Getting Data from First Week


def remove_free_trail_students(data):
    return [i for i in data if i[qkey] in paid_students]


def within_week(engagement_record):
    engagement_date = engagement_record["utc_date"]
    join_date = paid_students[engagement_record[qkey]]
    return (engagement_date - join_date).days < 7


paid_enrollments = remove_free_trail_students(non_udacity_enrollments)
paid_engagement = remove_free_trail_students(non_udacity_engagement)
paid_submissions = remove_free_trail_students(non_udacity_submissions)

paid_engagement_in_first_week = [
    engagement_record for engagement_record in paid_engagement
    if within_week(engagement_record)
]
print "Length paid_engagement_in_first_week:", len(
    paid_engagement_in_first_week)

# Exploring Student Engagement

from collections import defaultdict

engagement_by_account = defaultdict(list)
for engagement_record in paid_engagement_in_first_week:
    account_key = engagement_record['account_key']
    engagement_by_account[account_key].append(engagement_record)
total_minutes_by_account = {}
for account_key, engagement_for_student in engagement_by_account.items():
    total_minutes = 0
    for engagement_record in engagement_for_student:
        total_minutes += engagement_record['total_minutes_visited']
    total_minutes_by_account[account_key] = total_minutes
import numpy as np
total_minutes = total_minutes_by_account.values()
print 'Mean:', np.mean(total_minutes)
print 'Standard deviation:', np.std(total_minutes)
print 'Minimum:', np.min(total_minutes)
print 'Maximum:', np.max(total_minutes)

# Debugging Data Analysis Code

# reminder: https://wiki.python.org/moin/HowTo/Sorting#line-31
outlier = max(total_minutes_by_account.items(), key=lambda x: x[1])[0]
outlier_engagement_records = [
    engagement for engagement in paid_engagement_in_first_week
    if engagement[qkey] == outlier
]
print "Outlier records:", len(outlier_engagement_records)
period = outlier_engagement_records[-1]["utc_date"] - \
    outlier_engagement_records[0]["utc_date"]
print "Period:", period.days+1, "days"
